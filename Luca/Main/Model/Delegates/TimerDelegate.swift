import Foundation

public protocol TimerDelegate {
    
    func timerDidTick()
    
}
